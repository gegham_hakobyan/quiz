<?php

use Illuminate\Database\Seeder;
use App\Models\QuizQuestion;

class QuizQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(QuizQuestion::class, 15)->create()->each(static function ($quizQuestion) {
            $quizQuestion->save();
        });
    }
}
