<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('languages')->insert([
           'key' => 'arm',
           'name' => 'Armenian',
       ]);

        DB::table('languages')->insert([
            'key' => 'en',
            'name' => 'English',
        ]);

        DB::table('languages')->insert([
            'key' => 'ru',
            'name' => 'Russian',
        ]);

        DB::table('languages')->insert([
            'key' => 'fr',
            'name' => 'French',
        ]);

        DB::table('languages')->insert([
            'key' => 'de',
            'name' => 'Deutch',
        ]);
    }
}
