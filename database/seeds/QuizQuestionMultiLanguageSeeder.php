<?php

use Illuminate\Database\Seeder;
use App\Models\QuizQuestionMultiLanguage;

class QuizQuestionMultiLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(QuizQuestionMultiLanguage::class, 45)->create()->each(static function ($quizQuestionMultiLanguage) {
            $quizQuestionMultiLanguage->save();
        });
    }
}
