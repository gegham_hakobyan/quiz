<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(QuizSeeder::class);
         $this->call(LanguageSeeder::class);
         $this->call(QuizQuestionSeeder::class);
         $this->call(QuizQuestionAnswerSeeder::class);
         $this->call(QuizQuestionAnswerMultiLanguageSeeder::class);
         $this->call(QuizQuestionMultiLanguageSeeder::class);

    }
}
