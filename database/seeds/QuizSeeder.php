<?php

use Illuminate\Database\Seeder;
use \App\Models\Quiz;

class QuizSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Quiz::class, 5)->create()->each(static function ($quiz) {
            $quiz->save();
        });
    }
}
