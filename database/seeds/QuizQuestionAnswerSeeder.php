<?php

use Illuminate\Database\Seeder;
use App\Models\QuizQuestionAnswer;

class QuizQuestionAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(QuizQuestionAnswer::class, 60)->create()->each(static function ($answer) {
            $answer->save();
        });
    }
}
