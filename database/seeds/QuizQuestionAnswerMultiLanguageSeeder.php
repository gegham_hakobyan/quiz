<?php

use Illuminate\Database\Seeder;
use App\Models\QuizQuestionAnswerMultiLanguage;

class QuizQuestionAnswerMultiLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(QuizQuestionAnswerMultiLanguage::class, 180)->create()->each(static function ($quizQuestionAnswerMultiLanguage) {
            $quizQuestionAnswerMultiLanguage->save();
        });
    }
}
