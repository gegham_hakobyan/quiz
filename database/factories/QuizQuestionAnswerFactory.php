<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\QuizQuestionAnswer;
use Faker\Generator as Faker;

$factory->define(QuizQuestionAnswer::class, function (Faker $faker) {
    return [
        'question_id' => 1
    ];
});
