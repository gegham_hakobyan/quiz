<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\QuizQuestionMultiLanguage;
use Faker\Generator as Faker;

$factory->define(QuizQuestionMultiLanguage::class, function (Faker $faker) {
    return [
        'question_id' => 1,
        'language_id' => 1,
        'title' => $faker->sentence(10),
    ];
});
