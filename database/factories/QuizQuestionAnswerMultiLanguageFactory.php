<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\QuizQuestionAnswerMultiLanguage;
use Faker\Generator as Faker;

$factory->define(QuizQuestionAnswerMultiLanguage::class, function (Faker $faker) {
    return [
        'answer_id' => 1,
        'language_id' => 1,
        'answer' => $faker->sentence(10),
    ];
});
