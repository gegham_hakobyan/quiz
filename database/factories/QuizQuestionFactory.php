<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\QuizQuestion;
use Faker\Generator as Faker;

$factory->define(QuizQuestion::class, function (Faker $faker) {
    return [
        'quiz_id' => 1
    ];
});
