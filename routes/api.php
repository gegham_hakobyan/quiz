<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('quiz')->group(static function () {
    Route::get('/{quizId}', 'QuizController@getQuiz')->where('quizId', '[0-9]+');
    Route::get('/{quizId}/questions', 'QuizQuestionController@getQuizQuestions');
    Route::get('/question/{questionId}', 'QuizQuestionController@getQuizQuestion')->where('questionId', '[0-9]+');
    Route::get('/languages', 'LanguageController@getLanguages');
    Route::get('/languages/{quizId}', 'LanguageController@getQuizLanguages');
    Route::post('/add', 'QuizController@addQuiz');
    Route::put('/update/{quizId}', 'QuizController@updateQuiz');
    Route::get('/multi-language-question/{questionId}', 'QuizQuestionController@getMultiLanguageQuestions');
    Route::put('/multi-language-question/{questionId}', 'QuizQuestionController@updateMultiLanguageQuestions');
    Route::put('/multi-language-question/{quizId}/add', 'QuizQuestionController@addMultiLanguageQuestions');
});

Route::get('/quizzes', 'QuizController@getQuizzes');

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@authenticate');

Route::group(['middleware' => ['jwt.verify']], static function() {
    Route::get('user', 'UserController@getAuthenticatedUser');
});
