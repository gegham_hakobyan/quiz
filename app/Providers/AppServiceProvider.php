<?php

namespace App\Providers;

use App\Repositories\LanguageRepository;
use App\Repositories\QuizQuestionAnswerRepository;
use App\Repositories\QuizQuestionRepository;
use App\Repositories\QuizRepository;
use App\Services\LanguageService;
use App\Services\QuizQuestionAnswerService;
use App\Services\QuizQuestionService;
use App\Services\QuizService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    private function registerQuizService(): void
    {
        $this->app->singleton(QuizService::class, static function() {
            return new QuizService(resolve(QuizRepository::class));
        });
    }

    private function registerLanguageService(): void
    {
        $this->app->singleton(LanguageService::class, static function() {
            return new LanguageService(resolve(LanguageRepository::class));
        });
    }

    private function registerQuizQuestionService(): void
    {
        $this->app->singleton(QuizQuestionService::class, static function() {
            return new QuizQuestionService(resolve(QuizQuestionRepository::class));
        });
    }

    private function registerQuizAnswerService(): void
    {
        $this->app->singleton(QuizQuestionAnswerService::class, static function() {
            return new QuizQuestionAnswerService(resolve(QuizQuestionAnswerRepository::class));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->registerQuizService();

        $this->registerLanguageService();

        $this->registerQuizQuestionService();

        $this->registerQuizAnswerService();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
