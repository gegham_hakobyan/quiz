<?php

namespace App\Providers;

use App\Repositories\LanguageRepository;
use App\Repositories\QuizQuestionAnswerRepository;
use App\Repositories\QuizQuestionRepository;
use App\Repositories\QuizRepository;
use Illuminate\Support\ServiceProvider;

class AppRepositoryProvider extends ServiceProvider
{

    private function registerQuizRepository(): void
    {
        $this->app->singleton(QuizRepository::class, static function () {
            return new QuizRepository();
        });
    }

    private function registerLanguageRepository(): void
    {
        $this->app->singleton(LanguageRepository::class, static function () {
            return new LanguageRepository();
        });
    }

    private function registerQuizQuestionRepository(): void
    {
        $this->app->singleton(QuizQuestionRepository::class, static function () {
            return new QuizQuestionRepository();
        });
    }

    private function registerQuestionAnswerRepository(): void
    {
        $this->app->singleton(QuizQuestionAnswerRepository::class, static function () {
            return new QuizQuestionAnswerRepository();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerQuizRepository();

        $this->registerLanguageRepository();

        $this->registerQuizQuestionRepository();

        $this->registerQuestionAnswerRepository();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
