<?php

namespace App\Services;

use App\Models\Quiz;
use App\Models\QuizLanguage;
use App\Repositories\QuizRepository;
use Illuminate\Support\Collection;

class QuizService
{
    /** @var QuizRepository */
    private $quizRepository;

    /**
     * QuizService constructor.
     * @param QuizRepository $quizRepository
     */
    public function __construct(QuizRepository $quizRepository)
    {
        $this->quizRepository = $quizRepository;
    }

    /**
     * @return QuizRepository
     */
    public function getRepository(): QuizRepository
    {
        return $this->quizRepository;
    }

    /**
     * @return Collection
     */
    public function getItems(): Collection
    {
        return $this->getRepository()->getItems();
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getItem($id)
    {
        return $this->getRepository()->getItem($id);
    }

    /**
     * Return array from quiz model with languages
     *
     * @param $quiz
     * @return array
     */
    public function getArrayItem($quiz): array
    {
        $languages = $quiz->offsetGet('languages')->toArray();
        $quizArr = $quiz->toArray();
        $quizArr['languages'] = $languages;
        return $quizArr;
    }

    /**
     * Adds quiz to database and returns it
     *
     * @param array $params
     * @return Quiz
     */
    public function addQuiz(array $params): Quiz
    {
        return $this->getRepository()->addQuiz($params);
    }

    /**
     * Adds quiz language and returns it
     *
     * @param int $quizId
     * @param int $languageId
     * @return QuizLanguage
     */
    public function addQuizLanguage(int $quizId, int $languageId): QuizLanguage
    {
        return $this->getRepository()->addQuizLanguage($quizId, $languageId);
    }

    /**
     * Updates quiz
     *
     * @param array $params
     * @return mixed
     */
    public function updateQuiz(array $params)
    {
        return $this->getRepository()->updateQuiz($params);
    }

    /**
     * @param int $quizId
     * @param array $quizLanguages
     * @return mixed
     */
    public function removeChangedExistingLanguages(int $quizId, array $quizLanguages)
    {
        return $this->getRepository()->removeChangedExistingLanguages($quizId, $quizLanguages);
    }

    /**
     * @param int $quizId
     * @return mixed
     */
    public function getQuizLanguageIdsByQuizId(int $quizId)
    {
        return $this->getRepository()->getQuizLanguageIdsByQuizId($quizId);
    }
}
