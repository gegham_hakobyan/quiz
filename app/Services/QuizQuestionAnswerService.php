<?php

namespace App\Services;

use App\Models\QuizQuestionAnswer;
use App\Models\QuizQuestionAnswerMultiLanguage;
use App\Repositories\QuizQuestionAnswerRepository;

class QuizQuestionAnswerService
{
    /** @var QuizQuestionAnswerRepository */
    private $quizQuestionAnswerRepository;

    /**
     * QuizQuestionAnswerService constructor.
     * @param QuizQuestionAnswerRepository $quizQuestionAnswerRepository
     */
    public function __construct(QuizQuestionAnswerRepository $quizQuestionAnswerRepository)
    {
        $this->quizQuestionAnswerRepository = $quizQuestionAnswerRepository;
    }

    /**
     * @return QuizQuestionAnswerRepository
     */
    public function getRepository(): QuizQuestionAnswerRepository
    {
        return $this->quizQuestionAnswerRepository;
    }

    /**
     * @param int $answerId
     * @return mixed
     */
    public function getMultiLanguageAnswers(int $answerId)
    {
        return $this->getRepository()->getMultiLanguageAnswers($answerId);
    }

    /**
     * @param array $answers
     */
    public function updateItems(array $answers): void
    {
        foreach ($answers as $answer) {
            $this->updateItem($answer);
        }
    }

    /**
     * @param int $questionId
     * @param array $answers
     */
    public function addItems(int $questionId, array $answers): void
    {
        $quizQuestionAnswer = new QuizQuestionAnswer();

        $quizQuestionAnswer->offsetSet('question_id', $questionId);
        $quizQuestionAnswer->save();

        $multiLanguageAnswer = new QuizQuestionAnswerMultiLanguage();

        $multiLanguageAnswer->offsetSet('answer', $answers['answer']);
        $multiLanguageAnswer->save();

        $quizQuestionAnswer->offsetSet('is_correct', $answers['isCorrect']);
        $quizQuestionAnswer->save();
    }

    /**
     * @param array $answers
     */
    private function updateItem(array $answers): void
    {
        $multiLanguageAnswer = QuizQuestionAnswerMultiLanguage::find($answers['id']);

        $multiLanguageAnswer->offsetSet('answer', $answers['answer']);
        $multiLanguageAnswer->save();

        $quizQuestionAnswer = $multiLanguageAnswer->offsetGet('answer');
        $quizQuestionAnswer->offsetSet('is_correct', $answers['isCorrect']);
        $quizQuestionAnswer->save();
    }
}
