<?php

namespace App\Services;

use App\Repositories\LanguageRepository;

class LanguageService
{
    /** @var LanguageRepository  */
    private $languageRepository;

    /**
     * LanguageService constructor.
     * @param LanguageRepository $languageRepository
     */
    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    /**
     * @return LanguageRepository
     */
    public function getRepository(): LanguageRepository
    {
        return $this->languageRepository;
    }

    /**
     * Returns all languages
     *
     * @return array
     */
    public function getLanguages(): array
    {
        return $this->getRepository()->getLanguages();
    }

    /**
     * Returns quiz languages array
     *
     * @param int $quizId
     * @return array
     */
    public function getQuizLanguages(int $quizId): array
    {
        return $this->getRepository()->getQuizLanguages($quizId);
    }
}
