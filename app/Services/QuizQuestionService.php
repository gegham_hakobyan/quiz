<?php

namespace App\Services;

use App\Models\QuizQuestion;
use App\Models\QuizQuestionMultiLanguage;
use App\Repositories\QuizQuestionRepository;
use Illuminate\Database\QueryException;

class QuizQuestionService
{
    /** @var QuizQuestionRepository */
    private $quizQuestionRepository;

    /**
     * QuizQuestionService constructor.
     * @param QuizQuestionRepository $quizQuestionRepository
     */
    public function __construct(QuizQuestionRepository $quizQuestionRepository)
    {
        $this->quizQuestionRepository = $quizQuestionRepository;
    }

    /**
     * @return QuizQuestionRepository
     */
    public function getRepository(): QuizQuestionRepository
    {
        return $this->quizQuestionRepository;
    }

    /**
     * Returns quiz questions with multi language
     *
     * @param int $quizId
     * @return mixed
     */
    public function getItemsByQuizId(int $quizId)
    {
        return $this->getRepository()->getItemsByQuizId($quizId);
    }


    /**
     * Returns formatted quiz questions
     *
     * @param $quizQuestions
     * @return array
     */
    public function getFormattedQuestionsArray($quizQuestions): array
    {
        $quizQuestionsArray = [];

        foreach ($quizQuestions as $question) {
            $quizQuestionsArray[] = [
                'quizId' => $question->offsetGet('quiz_id'),
                'id' => $question->offsetGet('id'),
                'title' => $question->offsetGet('multiLanguageQuestions')->toArray()
            ];
        }

        return $quizQuestionsArray;
    }

    /**
     * @param int $questionId
     * @return mixed
     */
    public function getQuestion(int $questionId)
    {
        return $this->getRepository()->getQuestion($questionId);
    }

    /**
     * Returns quiz question reformatted array
     *
     * @param $quizQuestion
     * @return array
     */
    public function getQuestionArray($quizQuestion): array
    {
        $questionId = (int)$quizQuestion->offsetGet('id');

        return [
            'id' => $questionId,
            'quizId' => (int)$quizQuestion->offsetGet('quiz_id'),
            'titles' => $multiLanguageQuestions = $this->getMultiLanguageQuestions($questionId)->toArray(),
            'answers' => $this->getAnswers($quizQuestion)
        ];
    }

    /**
     * Returns quiz question answers array
     *
     * @param $quizQuestion
     * @return array
     */
    private function getAnswers($quizQuestion): array
    {
        $answerService = resolve(QuizQuestionAnswerService::class);

        $answersArray = [];

        $answers = $quizQuestion->offsetGet('answers');

        foreach ($answers as $answer) {
            $answerId = $answer->offsetGet('id');
            $answersArray [] = [
                'id' => $answerId,
                'answer' => $answerService->getMultiLanguageAnswers((int)$answerId)->toArray()
            ];
        }

        return $answersArray;
    }

    /**
     * @param int $questionId
     * @return mixed
     */
    public function getMultiLanguageQuestions(int $questionId)
    {
        return $this->getRepository()->getMultiLanguageQuestions($questionId);
    }

    /**
     * @param $question
     * @return array
     */
    public function getMultiLanguageAnswers(QuizQuestion $question): array
    {
        $answers = $question->offsetGet('answers');

        $multiLanguageAnswers = [];

        foreach ($answers as $answer) {
            $multiLanguageAnswers [] = [
                'is_correct' => $answer->offsetGet('is_correct'),
                'answers' => $answer->offsetGet('multiLanguageAnswers')->toArray()
            ];
        }

        return $multiLanguageAnswers;
    }

    /**
     * Returns formatted multi language question array
     *
     * @param array $multiLanguageQuestions
     * @param array $multiLanguageAnswers
     * @return array
     */
    public function getFormattedMultiLanguageQuestion(array $multiLanguageQuestions, array $multiLanguageAnswers): array
    {
        $multiLanguageQuestionArray = [];

        foreach ($multiLanguageQuestions as $multiLanguageQuestion) {
            $languageId = $multiLanguageQuestion['language_id'];

            $answersArray = $this->getExactLanguageAnswers($multiLanguageAnswers, (int)$languageId);

            $multiLanguageQuestionArray [] = [
                'title' => $multiLanguageQuestion['title'],
                'languageId' => $languageId,
                'id' => $multiLanguageQuestion['id'],
                'answers' => $answersArray,
            ];
        }

        return $multiLanguageQuestionArray;
    }

    /**
     * Returns formatted multi language answers array by language id
     *
     * @param array $multiLanguageAnswers
     * @param int $languageId
     * @return array
     */
    public function getExactLanguageAnswers(array $multiLanguageAnswers, int $languageId): array
    {
        $answersArray = [];

        foreach ($multiLanguageAnswers as $multiLanguageAnswer) {
            $key = array_search($languageId, array_column($multiLanguageAnswer['answers'], 'language_id'), true);

            $answersArray[] = [
                'id' => $multiLanguageAnswer['answers'][$key]['id'],
                'answerId' => $multiLanguageAnswer['answers'][$key]['answer_id'],
                'answer' => $multiLanguageAnswer['answers'][$key]['answer'],
                'isCorrect' => $multiLanguageAnswer['is_correct']
            ];
        }

        return $answersArray;
    }

    /**
     * @param $questions
     * @throws \Exception
     */
    public function updateQuestions($questions)
    {
        $answerService = resolve(QuizQuestionAnswerService::class);

        foreach ($questions as $question) {
            try {
                $this->updateItem($question);
                $answerService->updateItems($question['answers']);
            } catch (QueryException $exception) {
                throw new \Exception($exception->getMessage());
            }
        }
    }

    /**
     * @param array $question
     */
    private function updateItem(array $question): void
    {
        $multiLanguageQuestion = QuizQuestionMultiLanguage::find($question['id']);
        $multiLanguageQuestion->offsetSet('title', $question['title']);
        $multiLanguageQuestion->save();
    }

    /**
     * @param int $quizId
     * @param array $questions
     * @throws \Exception
     */
    public function addQuestions(int $quizId, array $questions): void
    {
        $answerService = resolve(QuizQuestionAnswerService::class);

        foreach ($questions as $question) {
            try {
                $quizQuestion = $this->addItem($quizId, $question);
                $answerService->addItems((int)$quizQuestion->offsetGet('id'), $question['answers']);
            } catch (QueryException $exception) {
                throw new \Exception($exception->getMessage());
            }
        }
    }

    /**
     * @param int $quizId
     * @param array $question
     * @return QuizQuestion
     */
    private function addItem(int $quizId, array $question): QuizQuestion
    {
        $quizQuestion = new QuizQuestion();
        $quizQuestion->offsetSet('quiz_id', $quizId);
        $quizQuestion->save();

        $multiLanguageQuestion = new QuizQuestionMultiLanguage();
        $multiLanguageQuestion->offsetSet('title', $question['title']);
        $multiLanguageQuestion->offsetSet('question_id', $quizQuestion->offsetGet('id'));
        $multiLanguageQuestion->save();

        return $quizQuestion;
    }


}
