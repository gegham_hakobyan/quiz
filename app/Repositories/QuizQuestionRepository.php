<?php


namespace App\Repositories;


use App\Models\QuizQuestion;
use App\Models\QuizQuestionMultiLanguage;

class QuizQuestionRepository
{
    /**
     * Returns quiz questions with multi language
     *
     * @param $quizId
     * @return mixed
     */
    public function getItemsByQuizId($quizId)
    {
        return QuizQuestion::where('quiz_id', '=', $quizId)
            ->get();
    }

    /**
     * @param int $questionId
     * @return mixed
     */
    public function getQuestion(int $questionId)
    {
        return QuizQuestion::where('id', '=', $questionId)->first();
    }

    /**
     * @param int $questionId
     * @return mixed
     */
    public function getMultiLanguageQuestions(int $questionId)
    {
        return QuizQuestionMultiLanguage::select('language_id', 'title')
            ->where('question_id', '=', $questionId)
            ->get();
    }

}
