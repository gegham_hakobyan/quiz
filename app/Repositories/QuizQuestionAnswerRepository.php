<?php


namespace App\Repositories;


use App\Models\QuizQuestionAnswerMultiLanguage;
use App\Models\QuizQuestionMultiLanguage;

class QuizQuestionAnswerRepository
{
    /**
     * @param int $answerId
     * @return mixed
     */
    public function getMultiLanguageAnswers(int $answerId)
    {
        return QuizQuestionAnswerMultiLanguage::select('language_id', 'answer')
            ->where('answer_id', '=', $answerId)
            ->get();
    }
}
