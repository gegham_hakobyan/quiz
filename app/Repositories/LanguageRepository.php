<?php


namespace App\Repositories;


use App\Models\Language;
use Illuminate\Support\Facades\DB;

class LanguageRepository
{
    /**
     * Returns all languages
     *
     * @return array
     */
    public function getLanguages(): array
    {
        return DB::table('languages')
            ->select('id', 'name', 'key')
            ->get()
            ->toArray();
    }

    /**
     * Returns quiz languages array
     *
     * @param int $quizId
     * @return array
     */
    public function getQuizLanguages(int $quizId): array
    {
        return DB::table('quiz_languages')
            ->join('languages', 'languages.id', '=', 'quiz_languages.language_id')
            ->select('languages.id', 'languages.name', 'languages.key')
            ->where('quiz_id', '=', $quizId)
            ->get()
            ->toArray();
    }
}
