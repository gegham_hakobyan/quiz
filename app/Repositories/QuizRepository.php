<?php


namespace App\Repositories;


use App\Models\Quiz;
use App\Models\QuizLanguage;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class QuizRepository
{
    /**
     * @return Collection
     */
    public function getItems(): Collection
    {
        return Quiz::select('id', 'name', 'description')->get();
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getItem($id)
    {
        return Quiz::select('id', 'name', 'description')
            ->where('id', '=', $id)
            ->first();
    }

    /**
     * Adds quiz to database and returns it
     *
     * @param array $params
     * @return Quiz
     */
    public function addQuiz(array $params): Quiz
    {
        $quiz = new Quiz();

        $this->saveQuiz(new Quiz(), $params);

        return $quiz;
    }

    /**
     * Add quiz language
     *
     * @param int $quizId
     * @param int $languageId
     * @return QuizLanguage
     */
    public function addQuizLanguage(int $quizId, int $languageId): QuizLanguage
    {
        $quizLanguage = new QuizLanguage();

        $quizLanguage->offsetSet('quiz_id', $quizId);
        $quizLanguage->offsetSet('language_id', $languageId);

        $quizLanguage->save();

        return $quizLanguage;
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function updateQuiz(array $params)
    {
        $quiz = Quiz::find($params['id']);

        if ($quiz) {
           $this->saveQuiz($quiz, $params);
        }

        return $quiz;
    }

    /**
     * @param Quiz $quiz
     * @param array $params
     */
    private function saveQuiz(Quiz $quiz, array $params): void
    {
        $quiz->offsetSet('name', $params['name']);
        $quiz->offsetSet('description', $params['description']);

        $quiz->save();
    }

    public function getExistingLanguagesToRemove(int $quizId, array $quizLanguages)
    {
        return QuizLanguage::where('quiz_id', '=', $quizId)
            ->whereNotIn('language_id', $quizLanguages)
            ->delete();
    }

    /**
     * @param int $quizId
     * @param array $quizLanguages
     * @return mixed
     */
    public function removeChangedExistingLanguages(int $quizId, array $quizLanguages)
    {
        return QuizLanguage::where('quiz_id', '=', $quizId)
            ->whereNotIn('language_id', $quizLanguages)
            ->delete();
    }

    /**
     * @param int $quizId
     * @return mixed
     */
    public function getQuizLanguageIdsByQuizId(int $quizId)
    {
        return QuizLanguage::where('quiz_id', '=', $quizId)->pluck('id')->toArray();
    }
}
