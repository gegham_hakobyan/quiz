<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class QuizQuestion extends Model
{
    /** @var string  */
    protected $table = 'quiz_questions';

    /**
     * @return BelongsTo
     */
    public function quiz(): BelongsTo
    {
        return $this->belongsTo('App\Models\Quiz');
    }

    /**
     * @return HasMany
     */
    public function answers(): HasMany
    {
        return $this->hasMany('App\Models\QuizQuestionAnswer', 'question_id');
    }

    /**
     * @return HasMany
     */
    public function multiLanguageQuestions(): HasMany
    {
        return $this->hasMany('App\Models\QuizQuestionMultiLanguage', 'question_id');
    }
}
