<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Quiz extends Model
{
    /** @var string */
    protected $table = 'quizzes';

    /**
     * @return HasMany
     */
    public function questions(): HasMany
    {
        return $this->hasMany('App\Models\QuizQuestion');
    }

    /**
     * @return BelongsToMany
     */
    public function languages(): BelongsToMany
    {
        return $this->belongsToMany('App\Models\Language', 'quiz_languages');
    }
}
