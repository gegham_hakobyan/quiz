<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class QuizQuestionAnswer extends Model
{
    /** @var string  */
    protected $table = 'quiz_question_answers';

    /**
     * @return BelongsTo
     */
    public function question(): BelongsTo
    {
        return $this->belongsTo('App\Models\QuizQuestion');
    }

    /**
     * @return  HasMany
     */
    public function multiLanguageAnswers(): HasMany
    {
        return $this->hasMany('App\Models\QuizQuestionAnswerMultiLanguage', 'answer_id');
    }
}
