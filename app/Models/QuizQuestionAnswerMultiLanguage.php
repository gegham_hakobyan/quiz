<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class QuizQuestionAnswerMultiLanguage extends Model
{
    /** @var string */
    protected $table = 'quiz_question_answer_multi_languages';

    /**
     * @return BelongsTo
     */
    public function answer(): BelongsTo
    {
        return $this->belongsTo('App\Models\QuizQuestionAnswer');
    }
}
