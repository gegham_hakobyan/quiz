<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class QuizLanguage extends Model
{

    /** @var string  */
    protected $table = 'quiz_languages';

    /** @var array  */
    protected $fillable = ['quiz_id', 'language_id'];

    /**
     * @return BelongsTo
     */
    public function quiz(): BelongsTo
    {
        return $this->belongsTo('App\Models\Quiz');
    }
}
