<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class QuizQuestionMultiLanguage extends Model
{
    /** @var string  */
    protected $table = 'quiz_question_multi_languages';

    /**
     * @return BelongsTo
     */
    public function question(): BelongsTo
    {
        return $this->belongsTo('App\Models\QuizQuestion');
    }

}
