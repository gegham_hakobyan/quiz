<?php

namespace App\Http\Controllers;

use App\Services\QuizQuestionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class QuizQuestionController extends Controller
{
    /**
     * @param Request $request
     * @param $quizId
     * @return JsonResponse
     */
    public function getQuizQuestions(Request $request, $quizId): JsonResponse
    {
        $quizQuestions = $this->getService()->getItemsByQuizId((int)$quizId);

        $quizQuestionsArray = $this->getService()->getFormattedQuestionsArray($quizQuestions);

        return response()->json($quizQuestionsArray)->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * @param $questionId
     * @return JsonResponse
     */
    public function getQuizQuestion($questionId): JsonResponse
    {
        $quizQuestion = $this->getService()->getQuestion((int)$questionId);

        $quizQuestionArray = $this->getService()->getQuestionArray($quizQuestion);

        return response()->json($quizQuestionArray);
    }

    /**
     * @param Request $request
     * @param $questionId
     * @return JsonResponse
     */
    public function getMultiLanguageQuestions(Request $request, $questionId): JsonResponse
    {
        $question = $this->getService()->getQuestion($questionId);

        $multiLanguageQuestions = $question->offsetGet('multiLanguageQuestions')->toArray();

        $multiLanguageAnswers = $this->getService()->getMultiLanguageAnswers($question);

        $multiLanguageQuestionArray = $this->getService()->getFormattedMultiLanguageQuestion($multiLanguageQuestions, $multiLanguageAnswers);

        return response()->json($multiLanguageQuestionArray, 200);
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    public function updateMultiLanguageQuestions(Request $request): void
    {
        $questions = $request->input('questions');

        $this->getService()->updateQuestions($questions);
    }

    /**
     * @param Request $request
     * @param $quizId
     * @throws \Exception
     */
    public function addMultiLanguageQuestions(Request $request, $quizId): void
    {
        $questions = $request->input('questions');

        $this->getService()->addQuestions((int)$quizId, $questions);
    }



    /**
     * @return QuizQuestionService
     */
    public function getService(): QuizQuestionService
    {
        return resolve(QuizQuestionService::class);
    }
}
