<?php

namespace App\Http\Controllers;

use App\Services\LanguageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getLanguages(Request $request): JsonResponse
    {
        $languagesArray = $this->getService()->getLanguages();

        return response()->json($languagesArray, 200);
    }

    /**
     * Returns quiz languages array
     *
     * @param Request $request
     * @param $quizId
     * @return JsonResponse
     */
    public function getQuizLanguages(Request $request, $quizId): JsonResponse
    {
        $languagesArray = $this->getService()->getQuizLanguages((int)$quizId);

        return response()->json($languagesArray, 200);
    }

    /**
     * @return LanguageService
     */
    public function getService(): LanguageService
    {
        return resolve(LanguageService::class);
    }
}
