<?php

namespace App\Http\Controllers;

use App\Services\QuizService;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuizController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getQuizzes(Request $request): JsonResponse
    {
        $quizzes = $this->getService()->getItems();

        $quizzesArray = [];

        foreach ($quizzes as $quiz) {
            $quizzesArray [] = $this->getService()->getArrayItem($quiz);
        }

        return response()->json($quizzesArray, 200);

    }

    /**
     * @param Request $request
     * @param $quizId
     * @return JsonResponse
     */
    public function getQuiz(Request $request, $quizId): JsonResponse
    {
        $quiz = $this->getService()->getItem($quizId);

        if ($quiz) {
            $quizArrayItem = $this->getService()->getArrayItem($quiz);
            return response()->json($quizArrayItem, 200);
        }

        return response()->json(['message' => 'Quiz with id ' . $quizId . ' not found'], 404);
    }

    /**
     * @return QuizService
     */
    public function getService(): QuizService
    {
        return resolve(QuizService::class);
    }

    /**
     * Adds quiz to database
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function addQuiz(Request $request): JsonResponse
    {
        $this->validateRequest($request);

        $quiz = $this->getService()->addQuiz([
            'name' => $request->input('name'),
            'description' => $request->input('description')
        ]);

        foreach ($request->input('quizLanguages') as $languageId) {
            $this->getService()->addQuizLanguage((int)$quiz->offsetGet('id'), (int)$languageId);
        }

        return response()->json(['id' => $quiz->offsetGet('id')], 200);
    }

    /**
     * Updates Quiz
     *
     * @param Request $request
     * @param $quizId
     * @return JsonResponse
     */
    public function updateQuiz(Request $request, $quizId): JsonResponse
    {
        $this->validateRequest($request);

        $quiz = $this->getService()->updateQuiz([
            'id' => $quizId,
            'name' => $request->input('name'),
            'description' => $request->input('description')
        ]);

        $this->getService()->removeChangedExistingLanguages($quizId, $request->input('quizLanguages'));

        $existingQuizLanguageIds = $this->getService()->getQuizLanguageIdsByQuizId($quizId);

        foreach ($request->input('quizLanguages') as $languageId) {
            if (in_array($languageId, $existingQuizLanguageIds, true)) {
                continue;
            }
            $this->getService()->addQuizLanguage((int)$quiz->offsetGet('id'), (int)$languageId);
        }

        return response()->json(['id' => $quiz->offsetGet('id')], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function getValidator(Request $request): \Illuminate\Contracts\Validation\Validator
    {
        return Validator::make($request->all(), [
            'name' => 'required:unique:quizzes',
            'description' => 'required',
            'quizLanguages' => 'required',
        ]);
    }


    /**
     * @param Request $request
     */
    private function validateRequest(Request $request): void
    {
        $validator = $this->getValidator($request);

        if ($validator->fails()) {
            throw new ValidationException($validator->errors());
        }
    }
}
